# Aufgabe 3 Übungsblatt 3

import numpy as np
import sys

# Funktion zur Lagrange-Interpolation aus der letzten Übung
def lagrange(x, nodes, k):
    n = len(nodes)
    if k >= n:
        print("Error: Index k must be less than the number of nodes.")
        sys.exit(1)
    L_k_x = 1
    for j in range(n):
        if j != k:
            L_k_x *= (x - nodes[j]) / (nodes[k] - nodes[j])
    return L_k_x

def lagrange_interpolation(x, nodes, function_values):
    n = len(nodes)
    polynom = 0
    for k in range(n):
        L_k_x = lagrange(x, nodes, k)
        polynom += function_values[k] * L_k_x
    return polynom


# (i)
# Funktionsdefinition: f(x) = sin(x)
def f(x):
    return np.sin(x)


# Berechnung des Interpolationsfehlers in der Maximumsnorm
def compute_interpolation_error_max_norm(n, a, b, num_points=1001):
    # Äquidistante Stützstellen
    nodes = np.linspace(a, b, n)
    function_values = f(nodes)
    
    # Testpunkte
    x_values = np.linspace(a, b, num_points)
    actual_values = f(x_values)
    
    # Interpolierte Werte
    interpolated_values = np.array([lagrange_interpolation(x, nodes, function_values) for x in x_values])
    
    # Fehlerberechnung (Maximumsnorm)
    error = np.max(np.abs(actual_values - interpolated_values))
    return error

# Parameter
a, b = -5, 5
max_n = 100

errors = []

# Berechnung des Interpolationsfehlers für verschiedene Werte von n
for n in range(1, max_n + 1):
    error = compute_interpolation_error_max_norm(n, a, b)
    errors.append(error)



# (ii)
# Funktionsdefinition: f(x) = 1 / (1 + x^2)
def f(x):
    return 1 / (1 + x**2)

# Berechnung des Interpolationsfehlers in der Maximumsnorm
def compute_interpolation_error_max_norm(n, a, b, num_points=1001):
    # Äquidistante Stützstellen
    nodes = np.linspace(a, b, n)
    function_values = f(nodes)
    
    # Testpunkte
    x_values = np.linspace(a, b, num_points)
    actual_values = f(x_values)
    
    # Interpolierte Werte
    interpolated_values = np.array([lagrange_interpolation(x, nodes, function_values) for x in x_values])
    
    # Fehlerberechnung (Maximumsnorm)
    error = np.max(np.abs(actual_values - interpolated_values))
    return error

# Parameter
a, b = -5, 5
max_n = 100

errors = []

# Berechnung des Interpolationsfehlers für verschiedene Werte von n
for n in range(1, max_n + 1):
    error = compute_interpolation_error_max_norm(n, a, b)
    errors.append(error)